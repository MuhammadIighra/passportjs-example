const PORT = process.env.PORT || 3000;

const express = require('express');

var app = express();

app.use(
    require('body-parser').urlencoded({
        extended:true
    })
);

app.get('/', (req, res) => {
    res.send('Welcome to the Homepage.')
})

//==================================================

const passport = require('passport');
app.use(passport.initialize());
app.use(passport.session());

app.get('/success', (req, res) => {
    res.send(`Welcome ${req.query.username}`)
});

app.get('/error', (req, res) => {
    res.send('Error logging in.');
});

passport.serializeUser(function (user, cb) {
    cb(null, user.id)
});

passport.deserializeUser(function (id, cb) {
    User.findById(id, function (err, user) {
        cb(err, user);
    })
})

//==================================================

const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://admin:admin@testdb-60cyz.mongodb.net/userInfo?retryWrites=true&w=majority', {useNewUrlParser:true});

mongoose.connection.on('error', function () {
    console.error("Failed to connect to database");
    process.exit(1);
});
mongoose.connection.once('open', function () {
    console.log("Connected to database");
});

const Schema = mongoose.Schema;

const UserDetail = new Schema({
    username: String,
    password: String
});

const UserDetails = mongoose.model('userInfo', UserDetail, 'userInfo');

//==================================================

// new UserDetails({
//     username: 'what',
//     password:'thefuck'
// }).save((idkwhat) => {
//     console.log(idkwhat)
// })

// UserDetails.find({}, 
//     (params) => {
//     console.log(params)
// });

//==================================================

const LocalStrategy = require('passport-local').Strategy;

passport.use(
    new LocalStrategy(function (username, password, done) {

        UserDetails.findOne({
            username: username
        }, (err, user) =>
        {
            if (err)
                {
                    console.log(err)
                    return done(err);
                }
            if (!user) 
                return done(null, false)
            if (user.password != password)
                return done(null, false);
            
            return done(null, user);
        })
    })
)

app.post('/login', passport.authenticate('local', { failureRedirect: '/error'}), (req, res) => {
    res.redirect('/success?username=' + req.user.username);
});

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});